# frozen_string_literal: true

require './lib/merkle_tree.rb'

# Build a MerkleTree
class MerkleTreeBuilder
  attr_accessor :data_blocks

  def initialize(data_blocks)
    self.data_blocks = data_blocks
  end

  def create_merkle_tree
    nodes = initialize_leafs

    merkle_tree = build_merkle_tree(nodes)

    merkle_tree
  end

  private

  def build_merkle_tree(nodes)
    while (node_count = nodes.count) != 1 # we reach the root
      new_nodes = []
      (0..(node_count - 1)).step(2) do |idx|
        left = nodes[idx]
        right = nodes[idx + 1] || left # duplicate left subtree (if needed) to balance tree
        new_nodes << MerkleTree.new(left: left, right: right)
      end
      nodes = new_nodes
    end
    nodes.first
  end

  def initialize_leafs
    data_blocks.map { |data_block| MerkleTree.new(data: data_block) }
  end
end
