# frozen_string_literal: true

require 'digest'

# Represent an full MerkleTree
class MerkleTree
  attr_accessor :left, :right, :root, :value

  def initialize(left: nil, right: nil, data: nil)
    if data
      self.root = compute_hash(data)
      # self.value = data # should not be kept, for debug purpose only!
    else
      params = [left, right]
      check_nodes! params

      self.left = left
      self.right = right

      self.root = compute_hash(params.map(&:root).join)
    end
  end

  def leaf?
    left.nil? && right.nil?
  end

  def to_s
    to_s_rec(self, 0)
  end

  def height
    sub_tree_height = left.nil? && right.nil? ? 0 : [left, right].map(&:height).max
    1 + sub_tree_height
  end

  def level(index)
    return [] if index > height

    nodes = [self]
    index.times do
      nodes = nodes.map { |node| [node.left, node.right] }.flatten
    end

    nodes.map(&:root)
  end

  private

  def compute_hash(data)
    ::Digest::SHA256.hexdigest(data.to_s)
  end

  def check_nodes!(params)
    params.each do |item|
      raise ArgumentError, "bad type: #{item.class.name} instead of MerkleTree" unless item.is_a?(MerkleTree)
    end
  end

  def to_s_rec(tree, tab)
    prefix = '  ' * tab

    if tree.leaf?
      "#{prefix}#{tree.root}" # (#{tree.value})"
    else
      "#{prefix}#{tree.root}\n" \
      "#{prefix}#{to_s_rec(tree.left, tab + 1)}\n" \
      "#{prefix}#{to_s_rec(tree.right, tab + 1)}"
    end
  end
end
