# frozen_string_literal: true

require './lib/merkle_tree_builder.rb'

RSpec.describe MerkleTreeBuilder do
  describe '#create_merkle_tree' do
    subject { described_class.new(params).create_merkle_tree }

    context 'when leaf count is even' do
      let(:params) { %w[1 2 3 4] }

      it { is_expected.to be_a(MerkleTree) }

      it 'must have the correct root' do
        expect(subject.root).to eq '85df8945419d2b5038f7ac83ec1ec6b8267c40fdb3b1e56ff62f6676eb855e70'
      end
    end

    context 'when leaf count is odd' do
      let(:params) { [1, 2, 3, 4, 5] }

      it { is_expected.to be_a(MerkleTree) }

      it 'must have the correct root' do
        expect(subject.root).to eq 'c19ce1b23fc9057eb072011d793ce33a47bb6fc3fe4cf9bf5d8f737abd3be0cb'
      end
    end
  end
end
