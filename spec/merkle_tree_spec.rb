# frozen_string_literal: true

require './lib/merkle_tree.rb'
require './lib/merkle_tree_builder.rb'
require 'digest'

RSpec.describe MerkleTree do
  let(:merkle_tree) { MerkleTreeBuilder.new(merkle_tree_builder_params).create_merkle_tree }

  let(:root_of_1_2_3_4) { '85df8945419d2b5038f7ac83ec1ec6b8267c40fdb3b1e56ff62f6676eb855e70' }

  describe '#root' do
    subject { merkle_tree.root }

    context 'when tree is a leaf' do
      let(:merkle_tree_builder_params) { [1] }

      it { is_expected.to eq Digest::SHA256.hexdigest('1') }
    end

    context 'when tree is balanced' do
      let(:merkle_tree_builder_params) { %w[1 2 3 4] }

      it { is_expected.to eq root_of_1_2_3_4 }
    end
  end

  describe '#height' do
    subject { merkle_tree.height }

    context 'when tree is a leaf' do
      let(:merkle_tree_builder_params) { [1] }

      it { is_expected.to eq 1 }
    end

    context 'when tree has 4 leafs' do
      let(:merkle_tree_builder_params) { [2] * 4 }

      it { is_expected.to eq 3 }
    end

    context 'when tree has 5 leafs' do
      let(:merkle_tree_builder_params) { [3] * 5 }

      it { is_expected.to eq 4 }
    end

    context 'when tree has 42 leafs' do
      let(:merkle_tree_builder_params) { [4] * 42 }

      it { is_expected.to eq 7 }
    end
  end

  describe '#level' do
    subject { merkle_tree.level(level) }

    context 'with a balanced tree' do
      let(:merkle_tree_builder_params) { %w[1 2 3 4] }

      context 'at level 0' do
        let(:level) { 0 }

        it { is_expected.to eq [root_of_1_2_3_4] }
      end

      context 'at level 1' do
        let(:level) { 1 }

        it { is_expected.to eq %w[33b675636da5dcc86ec847b38c08fa49ff1cace9749931e0a5d4dfdbdedd808a 13656c83d841ea7de6ebf3a89e0038fea9526bd7f686f06f7a692343a8a32dca] }
      end
    end

    context 'with an unbalanced tree' do
      let(:merkle_tree_builder_params) { %w[1 2 3 4 5] }

      context 'at level 0' do
        let(:level) { 0 }

        it { is_expected.to eq ['c19ce1b23fc9057eb072011d793ce33a47bb6fc3fe4cf9bf5d8f737abd3be0cb'] }
      end

      context 'at level 1' do
        let(:level) { 1 }

        it { is_expected.to eq [root_of_1_2_3_4, 'd30fdaa341f427812d8c1a5d385a2183c5437890010879a4a0b3448488b50fb6'] }
      end
    end
  end
end
