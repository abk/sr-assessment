# Sorare back-end assessment

## General notes

The preceding release (f19137b) was a bit messy: unused constant, inaccurate tests description, missplitted tests.

I took a few minutes to fix these issues in this last release, and provide answers to additional questions in this README.

## Run tests

`rspec`

## Additional answers

1. Using the illustration above, let’s assume I know the whole Merkle tree.
   Someone gives me the L2 data block but I don’t trust them. How can I check if
L2 data is valid?

  1. You just need to compute `hash(L2)` and check that it is equals to `Hash
     0-1` (that you already know). A merkle tree validation is useless in this case.

2. I know only the L3 data block and the Merkle root. What is the minimum information needed to check that the L3 data block and the Merkle root belong to the same Merkle tree?

  1. Hash1-1 and Hash0 (along with L3 data block and the merkle root itself).

3. What are some Merkle tree use cases?

  1. (Quick & compact) proof of existence ;

  2. Distributed file system checks.

